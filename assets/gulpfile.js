var app, base, concat, directory, gulp, gutil, hostname, path, refresh, sass, uglify, imagemin, minifyCSS, del, browserSync, autoprefixer, gulpSequence, shell, sourceMaps, plumber;

var autoPrefixBrowserList = ['last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'];

//load all of our dependencies
//add more here if you want to include more libraries
gulp         = require('gulp');
gutil        = require('gulp-util');
concat       = require('gulp-concat');
uglify       = require('gulp-uglify');
sass         = require('gulp-sass');
sourceMaps   = require('gulp-sourcemaps');
minifyCSS    = require('gulp-minify-css');
autoprefixer = require('gulp-autoprefixer');
gulpSequence = require('gulp-sequence').use(gulp);
shell        = require('gulp-shell');
plumber      = require('gulp-plumber');
merge        = require('merge-stream');
flatmap      = require('gulp-flatmap');


gulp.task('styles', function() {
    var theme = gulp.src([
        'sass/init.scss'
    ])
        .pipe(plumber({
            errorHandler: function (err) {
            console.log(err);
            this.emit('end');
            }
        }))
        .pipe(sourceMaps.init())
        .pipe(sass({
                errLogToConsole: true,
                includePaths: [
                    'sass/'
                ]
        }))
        .pipe(autoprefixer({
            browsers: autoPrefixBrowserList,
            cascade:  true
        }))
        .on('error', gutil.log)
        .pipe(concat('style.css'))
        .pipe(sourceMaps.write())
        .pipe(gulp.dest('css/'))

        return merge(theme);
});


gulp.task('default', ['styles'], function() {
    //a list of watchers, so it will watch all of the following files waiting for changes
    gulp.watch('sass/**', ['styles']);
});

gulp.task('deploy', ['styles-deploy'], function() {
    //a list of watchers, so it will watch all of the following files waiting for changes
    gulp.watch('sass/**', ['styles-deploy']);
});
