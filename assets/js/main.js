//DADOS JSON
var d = [ 
    { 
        "id": 1,
        "foto": "foto-alberto.png",
        "nome": "Alberto",
        "cargo": "Presidente",
        "idade": 20,
        "notificacao": 1
    },
    { 
        "id": 2,
        "foto": "foto-bruno.png",
        "nome": "Bruno",
        "cargo": "Diretor",
        "idade": 21,
        "notificacao": 2
    },
    { 
        "id": 3,
        "foto": "foto-helena.png",
        "nome": "Helena",
        "cargo": "Gerente",
        "idade": 22,
        "notificacao": 3
    },
    { 
        "id": 4,
        "foto": "foto-fernanda.png",
        "nome": "Fernanda",
        "cargo": "Desenvolvedor",
        "idade": 23,
        "notificacao": 4
    },
    { 
        "id": 5,
        "foto": "foto-diego.png",
        "nome": "Diego",
        "cargo": "Desenvolvedor",
        "idade": 24,
        "notificacao": 5
    },
    { 
        "id": 6,
        "foto": "foto-iris.png",
        "nome": "Iris",
        "cargo": "Desenvolvedor",
        "idade": 25,
        "notificacao": 6
    },
    { 
        "id": 7,
        "foto": "foto-hugo.png",
        "nome": "Hugo",
        "cargo": "Desenvolvedor",
        "idade": 26,
        "notificacao": 7
    },
    { 
        "id": 8,
        "foto": "foto-gustavo.png",
        "nome": "Gustavo",
        "cargo": "Desenvolvedor",
        "idade": 27,
        "notificacao": 5
    },
    { 
        "id": 9,
        "foto": "foto-gabriel.png",
        "nome": "Gabriel",
        "cargo": "Desenvolvedor",
        "idade": 28,
        "notificacao": 6
    }
]
///////


var people_list = document.getElementById('people-list');
var person_selected = null;

for(var k in d) {
    if (k == 0) { person_selected = 'active' } else { person_selected = '' };
    people_list.innerHTML += "<div class='person-card " + person_selected + "' id='" + d[k].id + "' onclick='setActive(this)'><div class='image-wrapper'><img src='/commcepta/assets/images/" + d[k].foto + "' class='actual-person-image'><div class='notification-counter'>" + d[k].notificacao + "</div></div><div class='info-wrapper-mini'><p class='info-item'>" + d[k].nome + "</p><p class='info-item role'>" + d[k].cargo + "</p></div></div>";
};

function setActive(e) {
    var id = null;
    for(var k in d) {
        document.getElementById(d[k].id).classList.remove('active');
        e.classList.add('active');
        id = parseInt(e.id) - 1;
    }
    document.getElementById('actual_person_name').innerHTML = d[id].nome;
    document.getElementById('actual_person_role').innerHTML = d[id].cargo;
    document.getElementById('actual_person_age').innerHTML = d[id].idade;
    document.getElementById('actual_person_img').src = '/commcepta/assets/images/' + d[id].foto;
};

//menu mobile

function toggleMobile() {
    var menu = document.getElementById('main-menu')
    if (menu.style.display === "none") {
        menu.style.display = "block";
    } else {
        menu.style.display = "none";
    }
}